import { createStore, combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import clientsReducer from "./app/reducer.jsx";
const reducer = combineReducers({
    clients: clientsReducer,
    form: reduxFormReducer, // mounted under "form"

});
const store = (window.devToolsExtension
    ? window.devToolsExtension()(createStore)
    : createStore)(reducer);

export default store;
