import React, {Fragment, useState} from 'react'
import '../../App.css'

class TrainerList extends React.Component {
    state = {trainers:[]}

    componentDidMount() {
        fetch(`http://localhost:5000/trainer`, {
            method: 'GET',
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({trainers: json});//
            });
    }

    render(){
        return (
            <Fragment>
                {" "}
                <div className="small_header">All Trainer</div>

                <div className='products_content'>
                    {this.state.trainers.map(trainer => (
                        <div className="products_block">
                            <p>{trainer.full_name}</p>
                            <p>{trainer.number_tekephone}</p>
                            <p>{trainer.specialization}</p>
                            <p>{trainer.salary}$</p>
                            <p>{trainer.adress}</p>

                        </div>
                    ))}

                </div>
            </Fragment>
        );
    }

};

export default TrainerList;