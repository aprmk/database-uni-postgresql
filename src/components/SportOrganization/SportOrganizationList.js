import React, {Fragment, useState} from 'react'
import '../../App.css'


class  SportOrganizationList extends React.Component {
    state = {sport_organizations:[]}

    componentDidMount() {
        fetch(`http://localhost:5000/sport_organization`, {
            method: 'GET',
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({sport_organizations: json});//.state.groups = json;
            });
    }
    render(){
        return (
            <Fragment>
                {" "}
                <div className="small_header">All Organization</div>

                <div className="main_block">
                    <div className='block_tittle'>
                        <p className="block_tittle_name">Name</p>
                        <p className="block_tittle_name">Type Sport</p>
                    </div>

                    <div className='products_content'>
                        {this.state.sport_organizations.map(sport_organization => (
                            <div>
                                <p>{sport_organization.name_organization}</p>
                                <p>{sport_organization.type_sport}</p>

                            </div>
                        ))}

                    </div>
                </div>
            </Fragment>
        );
    }

};

export default SportOrganizationList;