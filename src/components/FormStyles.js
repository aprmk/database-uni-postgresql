export const styles = {
    input: {
        padding: '5px 10px',
        border: '0',
        boxShadow: '0 0 15px 4px rgba(0,0,0,0.06)',
        fontSize: '13px'
    }
}