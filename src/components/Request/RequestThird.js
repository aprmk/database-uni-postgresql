import React, {Fragment, useState} from 'react'
import '../../App.css'

class RequestThird extends React.Component {
    state = {clients:[]}

    componentDidMount() {
        fetch(`http://localhost:5000/request_third`, {
            method: 'GET',
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({clients: json});
            });
    }

    render(){
        return (
            <Fragment>
                {" "}
                <div className="small_header">Клієнти, вік яких перевищує 16</div>

                <div className='products_content'>
                    {this.state.clients.map(client => (
                        <div className="products_block_request">
                            <p>{client.full_name}</p>
                            <p>{client.years}</p>
                        </div>
                    ))}

                </div>
            </Fragment>
        );
    }

};

export default RequestThird;