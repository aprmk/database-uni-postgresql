import React, {Fragment, useState} from 'react'
import '../../App.css'

class RequestFirst extends React.Component {
    state = {clients:[]}

    componentDidMount() {
        fetch(`http://localhost:5000/request_first`, {
            method: 'GET',
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({clients: json});//
            });
    }

    render(){
        return (
            <Fragment>
                {" "}
                <div className="small_header">Учні, які навчаються зa типом спорту «Chess»;</div>

                <div className='products_content'>
                    {this.state.clients.map(client => (
                        <div className="products_block_request">
                            <p>{client.full_name}</p>
                        </div>
                    ))}

                </div>
            </Fragment>
        );
    }

};

export default RequestFirst;