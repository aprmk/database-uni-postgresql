import React from 'react';
import ModalInput from "./ModalInput";


const InputClient = ({header}) => {

    return (
        <div>
            <div>
                <div>
                    <ModalInput header={header}/>
                </div>
            </div>
        </div>
    );
};

export default InputClient;