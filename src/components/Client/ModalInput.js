import React, {Fragment, useState} from 'react';
import Modal from "react-modal";
import {Field, reduxForm} from "redux-form";

let actions = require("../../app/actions.jsx");
let connect = require("react-redux").connect;

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
const ModalInput = props => {
    const {header, pristine, submitting} = props;

    const [modalIsOpen, setIsOpen] = React.useState(false);

    const [full_name, setFullName] = useState("")
    const [date_of_birth, setDateOfBirth] = useState("")
    const [town, setTown] = useState("")
    const [street, setStreet] = useState("")
    const [name_one_parent, setNameOneParent] = useState("")
    const [number_telephone_of_parent, setNumberTelephoneOfParent] = useState("")

    const onSubmitForm = async e => {
        e.preventDefault()
        try {
            const body = {full_name, date_of_birth, town, street, name_one_parent, number_telephone_of_parent };
            const response = await fetch(`http://localhost:5000/clients`,
                {
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(body)
                });

            const jsonData = await response.json();
            props.addClient(jsonData);
            closeModal();

            console.log(response)
        } catch (err) {
            console.log(err.message)
        }
    }

    function openModal() {
        setIsOpen(true);
    }


    function closeModal() {
        setIsOpen(false);
    }

    return (
        <Fragment>
            <div>

                <div className="input_product" onClick={openModal}>{header}</div>

                <Modal
                    isOpen={modalIsOpen}
                    onRequestClose={closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                    appElement={document.getElementById('root')}
                >
                    <div>Add new client</div>
                    <form className="form-input-modal" onSubmit={onSubmitForm}>
                        <br/>

                        <div>
                            <label>Full name</label>
                            <div>
                                <Field
                                    name="full_name"
                                    className="form-modal"
                                    component="input"
                                    placeholder="full_name"
                                    value={full_name}
                                    onChange={e => setFullName(e.target.value)}
                                />
                            </div>
                        </div>

                        <br/>

                        <div>
                            <label>Date of birth</label>
                            <div>
                                <Field
                                    name="date_of_birth"
                                    className="form-modal"
                                    component="input"
                                    placeholder="date_of_birth"
                                    value={date_of_birth}
                                    onChange={e => setDateOfBirth(e.target.value)}
                                />
                            </div>
                        </div>

                        <br/>

                        <div>
                            <label>Town</label>
                            <div>
                                <Field
                                    name="town"
                                    className="form-modal"
                                    component="input"
                                    placeholder="town"
                                    value={town}
                                    onChange={e => setTown(e.target.value)}
                                />
                            </div>
                        </div>

                        <br/>

                        <div>
                            <label>Street</label>
                            <div>
                                <Field
                                    name="street"
                                    className="form-modal"
                                    component="input"
                                    placeholder="street"
                                    value={street}
                                    onChange={e => setStreet(e.target.value)}
                                />
                            </div>
                        </div>

                        <br/>

                        <div>
                            <label>Name one paren</label>
                            <div>
                                <Field
                                    name="name_one_parent"
                                    className="form-modal"
                                    component="input"
                                    placeholder="name_one_parent"
                                    value={name_one_parent}
                                    onChange={e => setNameOneParent(e.target.value)}
                                />
                            </div>
                        </div>

                        <br/>

                        <div>
                            <label>Number telephone of parent</label>
                            <div>
                                <Field
                                    name="number_telephone_of_parent"
                                    className="form-modal"
                                    component="input"
                                    placeholder="number_telephone_of_parent"
                                    value={number_telephone_of_parent}
                                    onChange={e => setNumberTelephoneOfParent(e.target.value)}
                                />
                            </div>
                        </div>

                        <div>
                            <button className="form-button" type="submit" onSubmit={onSubmitForm}
                                    disabled={pristine || submitting}>Add
                            </button>
                            <button onClick={closeModal} className="form-button" type="submit" disabled={pristine || submitting}
                                    >
                                No Thanks
                            </button>
                        </div>

                    </form>

                </Modal>

            </div>
        </Fragment>

    );
};

function mapStateToProps(state) {
    return {
        products: state.products
    };
}

export default reduxForm({
    form: 'simple', // a unique identifier for this form
})(connect(mapStateToProps, actions)(ModalInput));