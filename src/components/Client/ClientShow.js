import React from 'react';

const ClientShow = ({client, onEdit}) => {
    function onClick(){
        console.log('on click')
        console.log(onEdit)
        onEdit();
    }
    return (
        <div>
            <p className="name">{client.full_name}</p>
            <p className="title">{client.date_of_birth}</p>
            <p className="title">{client.town}</p>
            <p className="title">{client.street}</p>
            <p className="title">{client.name_one_parent}</p>
            <p className="title">{client.number_telephone_of_parent}</p>
            <button className="product_button_edit"  onClick={onClick}>Edit</button>

        </div>
    );
};

export default ClientShow;