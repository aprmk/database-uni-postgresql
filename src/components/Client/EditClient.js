import React, {Fragment} from 'react';
import {Field, reduxForm} from "redux-form";

let actions = require("../../app/actions.jsx");
let connect = require("react-redux").connect;


const EditClient = ({handleSubmit, onSubmit}) => {

    return (
        <Fragment>
            <div>

                <form className="form-input-modal" onSubmit={handleSubmit(onSubmit)}>
                    <div>
                        <div>
                            <Field
                                name="full_name"
                                className="form-input-modal"
                                component="input"
                                placeholder="full name"
                            />
                        </div>
                        <br/>
                        <div>
                            <Field
                                name="date_of_birth"
                                className="form-input-modal"
                                component="input"
                                placeholder="date of birth"
                            />
                        </div>
                        <br/>
                        <div>
                            <Field
                                name="town"
                                className="form-input-modal"
                                component="input"
                                placeholder="town"
                            />
                        </div>
                        <br/>
                        <div>
                            <Field
                                name="street"
                                className="form-input-modal"
                                component="input"
                                placeholder="street"
                            />
                        </div>
                        <br/>
                        <div>
                            <Field
                                name="name_one_parent"
                                className="form-input-modal"
                                component="input"
                                placeholder="name one parent"
                            />
                        </div>
                        <br/>
                        <div>
                            <Field
                                name="number_telephone_of_parent"
                                className="form-input-modal"
                                component="input"
                                placeholder="number telephone of parent"
                            />
                        </div>
                        <br/>
                    </div>
                    <div>
                        <button className="product_button_upgrade"
                                type="submit"
                                onClick={handleSubmit(values =>
                                    onSubmit({
                                        ...values,
                                        action: 'update',
                                    }))}

                        >
                            Upgrade
                        </button>
                    </div>
                    <div>
                        <button className="product_button_delete"
                                type="submit"
                                onClick={handleSubmit(values =>
                                    onSubmit({
                                        ...values,
                                        action: 'delete',
                                    }))}
                        >
                            Delete
                        </button>
                    </div>
                </form>
            </div>
        </Fragment>
    );
};

export default reduxForm({
    form: 'client',

})(EditClient);