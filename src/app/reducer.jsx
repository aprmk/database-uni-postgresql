let Map = require("immutable").Map;

let reducer = function(state = Map(), action) {
    switch (action.type) {
        case "SET_STATE":
            return state.merge(action.state);
        case "ADD_CLIENT":
            return state.update("clients", (clients) => [...clients, action.client]);
        case "EDIT_CLIENT":
            return state.update("clients",
                clients => clients.map((client)=>client.id===action.client.id?action.client:client)
                );
        case "DELETE_CLIENT":
            return state.update("clients",
                clients => clients.filter((client) => client.id !== action.client.id)
            );
    }
    return state;
}
module.exports = reducer;